// <script src="gameframework.js"></script>
// then
// <script src="mgame.js"></script>
game = new Game();

game.MakeCube = function() {
    var texture =  new THREE.TextureLoader().load( 'vendor/threejs/textures/crate.gif' );
    var geometry = new THREE.BoxBufferGeometry( 200, 200, 200 );
    var material = new THREE.MeshLambertMaterial( { color: 0xffffff,  map: texture } );
    var CubeMeshCenter = new THREE.Mesh( geometry, material );
    CubeMeshCenter.receiveShadow = true;
    CubeMeshCenter.castShadow = true;
    // CubeMeshCenter

    var CubeMeshLeft = CubeMeshCenter.clone();
    CubeMeshLeft.position.x = -300;
    var CubeMeshRight = CubeMeshCenter.clone();
    CubeMeshRight.position.x = +300;

    this.AddToScene( "SCENE/MIDDLE", CubeMeshCenter );
    this.AddToScene( "SCENE/LEFT", CubeMeshLeft );
    this.AddToScene( "SCENE/RIGHT", CubeMeshRight );
}

game.AddStats = function() {
    this.stats = new Stats();
    this.container.appendChild( this.stats.dom );
}

// Override Game.Setup
game.Setup = function () {

    // Add Light1
    var white = new THREE.Color(0xffffff);
    var chartreuse = new THREE.Color("rgb(0, 95, 255)");
    var orange_yellow = new THREE.Color("rgb(255, 160, 0)");

    var light1 = new THREE.DirectionalLight( white, 1 );
    light1.position.set( 0, 0, 500 ).normalize();
    this.AddToScene("SCENE/LIGHT_1", light1 );
    

    var light2 = new THREE.DirectionalLight( orange_yellow, 1.5 );
    light2.position.set( 200, -200, 500 ).normalize();
    this.AddToScene("SCENE/LIGHT_2", light2 );

    var light3 = new THREE.DirectionalLight( chartreuse, 1.5 );
    light3.position.set( -200, 200, 500 ).normalize();
    this.AddToScene("SCENE/LIGHT_3", light3 );

    this.MakeCube();

    var cube = this.getObj("SCENE/MIDDLE")
    light1.target = cube;

    var cube = this.getObj("SCENE/LEFT")
    light2.target = cube;

    var cube = this.getObj("SCENE/RIGHT")
    light3.target = cube;

    this.AddStats();
}

// Override Game Update
game.Update = function (time_ms) {
    // USE MESH FROM THIS OBJECT ADD IN MakeCube
    CubeMeshes = [this.getObj("SCENE/LEFT"), this.getObj("SCENE/MIDDLE"), this.getObj("SCENE/RIGHT")];
    var mul = 0
    CubeMeshes.forEach( function (element) {
            element.rotation.x = time_ms * (1 + mul);
            element.rotation.y = time_ms * (1 + mul);
            mul = (mul * 2) + 2;
        });
    this.stats.update();
}

