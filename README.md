# Basic Electron App

Basic Demo for an Electron Application
It also forms a base for any development I may start with regards to Electron and Threejs in the future.


# Make Copy and make a new Repository
With this in mind then herewith a description on what to do use this project as a base for your next project.

1. First off clone this repository.
2. Delete the ".git" directory in your new local copy.
3. Create a repo at your favorite Git Hosting provider.
4. Copy git-prep.bat to git-init.bat
	* Edit git-init.bat as required, keep in mind to select "git config --global credential.helper wincred" if you are on windows and if not yet installed download and install [WiX Toolset build tools](http://wixtoolset.org/releases/).
	* Make sure you have the username and password for your git hosting provider ready.
	* Run git-init.bat
	* Check your online repository to make sure project was imported into your repo.
5. Once you have set up your own repo ensure you have _nodeJs_ LTS accompanying _npm_ installed available at https://nodejs.org.
6. Run dev-init.bat, which will download and install the required electron packages.
7. Run either dev-web.bat or dev-test.bat to develop your application. During development with dev-test, i.e. Electron, you can uncomment the line containing ``mainWindow.webContents.openDevTools()`` to ensure you can debug from inside electron.
8. Once you are happy ensure you have recommented ``mainWindow.webContents.openDevTools()`` and then run dev-build.bat to build for all supported platforms.

# Note
Please make sure every bat file you execute conform to the locations, usernames, etc. of your system software and service providers. For instance dev-run will execute game.exe for the win64 platform, the name of the application and directories may differ vastly depending on how you change the program. 
To this end feel free to change the package.json, main.js, index.html, and game.js as you require. Of note also is that only the content of the last few function is game.js need to be changed as they override the default Setup and Update functions of the _Game_ object instance in the _game_ variable.
