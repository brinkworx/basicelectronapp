// const {app, BrowserWindow} = require('electron');
const electron = require('electron');
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;

var screenElectron;
let mainWindow;

// This method will be called when Electron has done everything
// initialization and ready for creating browser windows.
function createWindow() {
  screenElectron = electron.screen;
  var mainScreen = screenElectron.getPrimaryDisplay();
  console.log(mainScreen);
  let width = mainScreen.workArea.width * 0.80;
  let height = mainScreen.workArea.width * 0.80;
  // Create the browser window.
  mainWindow = new BrowserWindow({
          width: width, 
          height: height, 
          frame:true, 
          transparent: false,
          backgroundColor: "rgba(0,0,0,0)",
  });

  //mainWindow.setIgnoreMouseEvents(true);

	// Uncomment to debug
	// mainWindow.webContents.openDevTools();

  // and load the index.html of the app.
  mainWindow.loadURL('file://' + __dirname + '/index.html');

  // Emitted when the window is closed.
  mainWindow.on('closed', function() {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null;
    });

}
  


// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready',  createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', function() {
  if (process.platform != 'darwin')
    app.quit();
});

app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (win === null) {
    createWindow();
  }
})
