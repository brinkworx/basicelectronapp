@echo Copy to git-prep.bat file to git-init.bat in the same directory.
@echo Then remove the remaining "@REM " statements below this text in git-init.bat
@echo Edit the remote repository details that you must have created before running git-init.bat
@echo It intended to allow you to create a new project in a different git repo.
@REM git init
@REM git add --all
@REM git commit -m "Initial Commit"
@REM git remote add origin https://<username>@bitbucket.org/<username>/basicelectronapp.git
@REM git config --global credential.helper 'cache --timeout 28800'
@REM git config --global credential.helper wincred
@REM git pull origin master --allow-unrelated-histories
@REM git push -u origin master