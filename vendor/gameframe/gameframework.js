
var _games = []; // In NameSpace
var Game = function () {
    return {
        container:null,
        camera:null,
        scene:null,
        renderer:null,
        objs:{},
        // Method Init
        Init: function (html_container) {
            this._InitCamSceneAndLightFog(html_container);
            
            // Precreate Render
            this.renderer = new THREE.WebGLRenderer( { antialias: false, alpha : true } );
            this.renderer.setClearColor(0x000000, 1);
            // renderer.setClearColor( scene.fog.color );
            this.renderer.setPixelRatio( window.devicePixelRatio );
            this.renderer.setSize( window.innerWidth, window.innerHeight );

            // renderer.gammaInput = true;
            // renderer.gammaOutput = true;

            this.Setup();
            //

            this.container.appendChild( this.renderer.domElement );

            //

            window.addEventListener( 'resize', this. onWindowResize, false );
        },
        // Virtual Game.Start Method Should be overloaded
        Setup: function() {
            console.log('Game.Start method should be overload.')
        },

        Begin : function() {
            if (_games === undefined) { 
                _games = []; 
            }
            var found = _games.find(function(element) {
                return element === this;
              });
            if(!found)
                _games.push(this); // Add to the array of games
            this._Animate()
        },
        Update : function( time_ms ) {
            if (this.debugUpdateMsg === undefined) {
                this.debugUpdateMsg = true;
                console.log('Game.Update method should be overload.')
            }
        },        
        AddToScene: function( name, obj ) {
            // Add Object By Name
            obj.name = name;
            this.objs[name] = obj;
            this.scene.add( obj );
        },
        getObj: function( objName ) {
            return this.objs[objName];
        },
        // Resize Method Can be overloaded in Start Method
        onWindowResize : function () {
            // Animate Does not Preserve it's 'this' variale so substiture with game global
            if(_games != undefined) {
                _games.forEach(function(_eachGame) {
                    if(_eachGame !== undefined) {
                        _eachGame.camera.aspect = window.innerWidth / window.innerHeight;
                        _eachGame.camera.updateProjectionMatrix();
                        _eachGame.renderer.setSize( window.innerWidth, window.innerHeight );
                    }
                });
            }
        },
        // PRIVATE METHODS
        _InitCamSceneAndLightFog: function (html_container) {
            // First Set Container
            this.container = html_container
            // Camera
            this.camera = new THREE.PerspectiveCamera( 27, window.innerWidth / window.innerHeight, 1, 3500 );
            this.camera.name = "SCENE/CAMERA";
            this.camera.position.z = 2750;
            this.objs[this.camera.name] = this.camera;
            // Scene
            this.scene = new THREE.Scene();
            this.scene.name = "SCENE";
            this.objs[this.scene.name] = this.scene;
            // Fog
            this.scene.fog = new THREE.Fog( 0x050505, 2000, 3500 );
            this.scene.fog.name = "SCENE/FOG"
            this.objs[this.scene.fog.name] = this.scene.fog;
            // AmbientLight
            this.AddToScene("SCENE/AMBIENT_LIGHT", new THREE.AmbientLight( 0x444444 ))
        },
        _Animate : function()  {
            // Animate Does not Preserve it's 'this' variale so substiture with game global
            if(_games != undefined) {
                _games.forEach(function(_eachGame) {
                    if(_eachGame !== undefined) {
                        requestAnimationFrame( _eachGame._Animate);
                        _eachGame._Render();
                    }
                });
            }
        },
        _Render : function() {
            var time_ms = Date.now() * 0.001;
            this.Update(time_ms);
            this.renderer.render( this.scene, this.camera );
        }
    };
}

